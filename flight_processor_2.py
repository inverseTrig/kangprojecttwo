def sort_flights(flights):
	if(len(flights_final) == 1):
		return flights
	else:
		for i in range(1,len(flights)):
			if(flights[i][1] < flights[i-1][2]):
				diff = flights[i-1][2] - flights[i][1] + 1
				flights[i][1] += diff
				flights[i][2] += diff
		return flights

flights_init = []
flights_final = []
with open("flights.txt") as f:
    for line in f:
        flights_init.append(line.strip('\n').split(','))

flights_init.sort(key = lambda row: (row[1], row[2]))

n = len(flights_init)

for i in range(0,int(flights_init[n-1][1])+1):
	print("We are in timeframe: ", i)
	for j in range(0,len(flights_init)):
		if (int(flights_init[j][1]) == i):
			print(i,"YES")
			flights_final.append([flights_init[j][0], int(flights_init[j][2]), int(flights_init[j][2]) + int(flights_init[j][3]) - 1])
			print("We added flight ", flights_init[j][0], " to the list.")
	flights_final = sort_flights(flights_final)
	print("Timeframe: ", i, flights_final)
	
print("\nOur final result is: ", flights_final)
